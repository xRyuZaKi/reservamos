import { AxiosRequestConfig } from 'axios';
import React, { useEffect, useState } from 'react'
import { ActivityIndicator, Button, FlatList, KeyboardAvoidingView, Pressable, Text, TextInput, View } from 'react-native';
import { useRequest } from './hooks/useRequest';
import { City, Weather } from './models/CityModel';
import { theme } from './theme/theme';

const Main = () => {

  const [query, setQuery] = useState<string>("");
  const [cities, setCities] = useState<City[]>([]);
  const [citiesWeather, setCitiesWeather] = useState<City[]>([]);

  const { isLoading, isSuccess, hasError, data, api } = useRequest<City[]>();
  const { isLoading: isLoadingWeather, isSuccess: isSuccessWeather, hasError: hasErrorWeather, data: dataWeather, api: apiWeather } = useRequest<City[]>();

  useEffect(() => {
    if (!isLoading && !hasError && isSuccess) {
      const response: City[] = (data as City[]).filter(value => value.result_type === "city");
      setCities(response);
    }
  }, [isLoading]);

  useEffect(() => {
    if (!isLoadingWeather && !hasErrorWeather && isSuccessWeather) {
      let tmpCitiesWeather = [...citiesWeather];

      const lat = dataWeather.lat;
      const lon = dataWeather.lon;
      const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

      tmpCitiesWeather.map((value) => {
        let weather: Weather[] = [];

        if (lat == parseFloat(value.lat).toFixed(4) && lon == parseFloat(value.long).toFixed(4)) {
          dataWeather.daily.forEach(data => {
            const dt = data.dt;
            const d = new Date(dt * 1000);

            const dayName = days[d.getDay()];
            const temp = data.temp;

            weather.push({
              dayName,
              weatherDay: temp
            });

            value.weather = weather;
          });
        }
      });

      setCitiesWeather(tmpCitiesWeather);
    }
  }, [isLoadingWeather]);

  const selectCity = (index: number) => {
    const city = citiesWeather.findIndex(e => e.id === cities[index].id);
    if (city === -1) {
      setCitiesWeather([...citiesWeather, cities[index]]);
      getCityWeather(cities[index].lat, cities[index].long);
      return;
    }
  };

  const getCityWeather = (lat, lon) => {
    const config: AxiosRequestConfig = {
      method: "GET",
      params: {
        lat,
        lon,
        units: "metric",
        exclude: "minutely,hourly,alerts"
      }
    }

    apiWeather.sendRequest("", config, 2);
  }

  const getCities = () => {
    const config: AxiosRequestConfig = {
      params: {
        q: query
      },
    }

    api.sendRequest("", config, 1);
  };

  const renderItemCity = (item: City, index: number) => {
    return (
      <View style={{
        flex: 1,
        flexWrap: "wrap",
        borderWidth: 1,
        borderRadius: 10,
        marginVertical: 5,
        marginHorizontal: 5,
      }}>
        <View style={{ flexDirection: "row", marginHorizontal: 5 }}>
          <Text style={{ flex: 1, flexWrap: 'wrap', color: theme.secondary }}>{`${item.state} - ${item.city_name}`}</Text>
        </View>
        <Pressable
          onPress={() => selectCity(index)}
          style={{
            backgroundColor: theme.secondary,
            width: 50,
            alignSelf: "center",
            marginVertical: 5,
            borderRadius: 5
          }}>
          <Text style={{ color: theme.primary, fontWeight: "bold", textAlign: "center" }}>Select</Text>
        </Pressable>
      </View>
    )
  };

  const renderItemCityWeather = (item: City) => {
    return (
      <View
        style={{
          flex: 1,
          borderWidth: 1,
          borderRadius: 5,
          marginVertical: 5,
          borderColor: theme.secondary,
        }}
      >
        <View style={{ flexDirection: "row", justifyContent: "space-around", marginVertical: 5 }}>
          <View style={{ alignItems: "center" }}>
            <Text style={{ color: theme.secondary, fontWeight: "bold" }}>State</Text>
            <Text style={{ color: theme.secondary }}>{item.state}</Text>
          </View>
          <View style={{ alignItems: "center" }}>
            <Text style={{ color: theme.secondary, fontWeight: "bold" }}>City</Text>
            <Text style={{ color: theme.secondary }}>{item.city_name}</Text>
          </View>
        </View>

        <FlatList
          data={item.weather}
          renderItem={({ item }) => renderWeather(item)}
          horizontal
        />
      </View>
    );
  };

  const renderWeather = (item: Weather) => {
    return (
      <View style={{
        flex: 1,
        borderWidth: 1,
        borderColor: theme.secondary,
        borderRadius: 5,
        margin: 5,
      }}>
        <View style={{ alignItems: "center", marginHorizontal: 5 }}>
          <Text style={{ color: theme.secondary, fontWeight: "bold" }}>{parseFloat(item.weatherDay?.day).toFixed(0)} °C</Text>
          <Text style={{ color: theme.secondary, fontWeight: "bold" }}>{item.dayName}</Text>
          <Text style={{ color: theme.secondary, fontWeight: "bold" }}>Max = {parseFloat(item.weatherDay?.max).toFixed(0)} °C</Text>
          <Text style={{ color: theme.secondary, fontWeight: "bold" }}>Min = {parseFloat(item.weatherDay?.min).toFixed(0)} °C</Text>
        </View>
      </View>
    );
  }

  return (
    <View style={{
      flex: 1,
      backgroundColor: theme.primary
    }}>
      <View style={{ flex: 1, marginHorizontal: 10 }}>
        <View style={{ flex: 1, marginTop: 15 }}>
          <View style={{ marginVertical: 5 }}>
            <Text style={{ color: theme.secondary, fontWeight: "bold", fontSize: 20 }}>Search city</Text>
          </View>
          <View style={{ marginVertical: 5 }}>
            <TextInput
              style={{
                color: theme.secondary,
                borderWidth: 1,
                borderColor: theme.secondary,
                borderRadius: 10
              }}
              onChangeText={(value) => setQuery(value)}
              onSubmitEditing={getCities}
            />
          </View>
          <View style={{ flexDirection: "row", justifyContent: "space-around" }}>
            <Pressable
              style={{
                justifyContent: "center",
                alignSelf: "center",
                backgroundColor: theme.secondary,
                height: 50,
                width: 80,
                borderRadius: 10
              }}
              onPress={getCities}
            >
              <Text style={{ color: theme.primary, fontWeight: "bold", textAlign: "center" }}>Search</Text>
            </Pressable>
            <Pressable
              style={{
                justifyContent: "center",
                alignSelf: "center",
                backgroundColor: theme.secondary,
                height: 50,
                width: 80,
                borderRadius: 10
              }}
              onPress={() => {
                setCities([]);
                setCitiesWeather([]);
              }}
            >
              <Text style={{ color: theme.primary, fontWeight: "bold", textAlign: "center" }}>Clear</Text>
            </Pressable>
          </View>
        </View>

        {
          isLoading
            ? <View style={{ flex: 1 }}>
              <ActivityIndicator size={30} color={theme.secondary} />
            </View>
            : <>
              <View style={{ flex: 3.5 }}>
                <View style={{ flex: 1, marginBottom: 10 }}>
                  <Text style={{ color: theme.secondary, fontWeight: "bold", fontSize: 20, }}>Cities</Text>
                  <FlatList
                    data={cities}
                    renderItem={({ item, index }) => renderItemCity(item, index)}
                    numColumns={2}
                    keyExtractor={item => item.id.toString()}
                  />
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={{ color: theme.secondary, fontWeight: "bold", fontSize: 20, }}>Weather cities</Text>
                  <FlatList
                    data={citiesWeather}
                    renderItem={({ item, index }) => renderItemCityWeather(item)}
                    keyExtractor={item => item.id.toString()}
                    contentContainerStyle={{
                      flexGrow: 1,
                    }}
                  />
                </View>
              </View>
            </>
        }
      </View>
    </View>
  );
}

export default Main;
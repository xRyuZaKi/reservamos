import axios from "axios";

const apiReq = {
	cities: axios.create({
		baseURL: "https://search.reservamos.mx/api/v2/places"
	}),
	weather: axios.create({
		baseURL: "https://api.openweathermap.org/data/2.5/onecall",
		params: {
			appid: "dcec7bfde65161b4e4f1e0959f53330a"
		}
	})
}

export default apiReq;